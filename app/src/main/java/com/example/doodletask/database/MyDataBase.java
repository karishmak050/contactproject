package com.example.doodletask.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.doodletask.model.ContactDetails;
import com.example.doodletask.model.SignupDetails;

import java.util.ArrayList;
import java.util.List;

public class MyDataBase {


    private MyHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;
    private Context mContext;
    public static final String DATABASE_NAME = "Contact_data";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "user_details";
    public static final String TABLE_NAME_SIGNUP = "signup_details";


    public static final String NAME = "name";
    public static final String NUMBER = "number";

//signup
    public static final String USERNAME = "username";
    public static final String USEREMAIL = "useremail";
    public static final String USERPASS = "userpass";



    public MyDataBase(Context mContext) {
        this.mContext = mContext;
        dbHelper = new MyHelper(mContext, DATABASE_NAME, null, DATABASE_VERSION);

    }

    /**
     * Open DataBase
     */
    public void open() {
        sqLiteDatabase = dbHelper.getWritableDatabase();
    }

    public void readOnly() {
        sqLiteDatabase = dbHelper.getReadableDatabase();
    }

    /**
     * Close dataBase
     */

    public void closeDataBase() {
        sqLiteDatabase.close();
    }

    /**
     * Insert Values
     */
    public void insertData(ContactDetails data, String loggedUserEmail) {
        open();
        try {

            ContentValues values = new ContentValues();
            values.put(NAME, data.getName());
            values.put(NUMBER, data.getContact());
            values.put(USEREMAIL, loggedUserEmail);
            sqLiteDatabase.insert(TABLE_NAME, null, values);

            Log.d("UserData","Inserted successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }

        closeDataBase();
    }


    // check for all available contacts
    public ArrayList<ContactDetails> readContacts(String loggedUserEmail) {
        ArrayList<ContactDetails> arrayList = new ArrayList<>();
        Cursor cursor;
        open();

        cursor = sqLiteDatabase.rawQuery("SELECT * from " + TABLE_NAME + " where "+USEREMAIL +" = '"+loggedUserEmail+"'",null );

        if (cursor != null && cursor.getCount() > 0) {

            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                ContactDetails contactDetails = new ContactDetails();
                contactDetails.setContact(cursor.getString(cursor.getColumnIndex(NUMBER)));
                contactDetails.setName(cursor.getString(cursor.getColumnIndex(NAME)));
                arrayList.add(contactDetails);

            }
        }
        Log.d("read","visible  "+cursor.getCount());

        closeDataBase();
        return arrayList;
    }

   public ContactDetails readContactByNumber(String loggedUserEmail){
       ContactDetails data = null;
        open();
        Cursor cursor;
        cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_NAME + " where " + USEREMAIL + " = '" + loggedUserEmail + "'", null);
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0){
            data = new ContactDetails();
            do {
                data.setContact(cursor.getString(cursor.getColumnIndex(NUMBER)));
                data.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            }while (cursor.moveToNext());
        }
        closeDataBase();
        return data;
    }

    public void deleteByNumber(String number){
        open();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE " +NUMBER+" = '"+number+"'";
        sqLiteDatabase.execSQL(query);
        closeDataBase();
    }

    public void updateDatabase(ContactDetails contactDetails, String oldNumber) {
        open();

        ContentValues cv = new ContentValues();
        cv.put(NAME,contactDetails.getName()); //These Fields should be your String values of actual column names
        cv.put(NUMBER,contactDetails.getContact());

        sqLiteDatabase.update(TABLE_NAME, cv, NUMBER+" = "+oldNumber, null);



        /*String query = TABLE_NAME + " SET "+ NAME+ "= "+"'"+contactDetails.getName()+"', "+ NUMBER +" = "+"'"+contactDetails.getContact() +"'"+
                " where " +NUMBER +"=" +"'"+oldNumber+"'";
        sqLiteDatabase.execSQL(query);*/
        closeDataBase();
    }

    public void insertUserSignupData(SignupDetails signupDetails) {
        open();
        try {

            ContentValues values = new ContentValues();
            values.put(USERNAME, signupDetails.getUserName());
            values.put(USEREMAIL, signupDetails.getUserEmail());
            values.put(USERPASS, signupDetails.getUserPassword());
            sqLiteDatabase.insert(TABLE_NAME_SIGNUP, null, values);

            Log.d("UserData","Inserted successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }

        closeDataBase();
    }

    public SignupDetails readSignUpDetails(String useremail){
        SignupDetails data = null;
        open();
        Cursor cursor;
        cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_NAME_SIGNUP + " where " + USEREMAIL + " = '" + useremail + "'", null);
        //cursor = sqLiteDatabase.rawQuery("select * from "+ TABLE_NAME_SIGNUP+"",null );
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0){
            data = new SignupDetails();
            do {

                data.setUserEmail(cursor.getString(cursor.getColumnIndex(USEREMAIL)));
                data.setUserPassword(cursor.getString(cursor.getColumnIndex(USERPASS)));
                Log.d("read","visible  "+cursor.getCount());
            }while (cursor.moveToNext());
        }
        Log.d("read","visible  "+cursor.getCount());

        closeDataBase();
        return data;
    }









}


