package com.example.doodletask.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.example.doodletask.database.MyDataBase.NAME;
import static com.example.doodletask.database.MyDataBase.NUMBER;
import static com.example.doodletask.database.MyDataBase.TABLE_NAME;
import static com.example.doodletask.database.MyDataBase.TABLE_NAME_SIGNUP;
import static com.example.doodletask.database.MyDataBase.USEREMAIL;
import static com.example.doodletask.database.MyDataBase.USERNAME;
import static com.example.doodletask.database.MyDataBase.USERPASS;

public class MyHelper extends SQLiteOpenHelper {

    public MyHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ( id integer," +NAME+" text, "+ NUMBER + " text primary key, "+USEREMAIL+ " text" + ")";
        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_NAME_SIGNUP + " ( id integer," +USERNAME+" text, "+USERPASS+" text, "+ USEREMAIL + " text primary key" + ")";

        sqLiteDatabase.execSQL(CREATE_TABLE);
        sqLiteDatabase.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
