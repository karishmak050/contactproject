package com.example.doodletask.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.doodletask.R;
import com.example.doodletask.model.ContactDetails;
import com.example.doodletask.screens.HomeActivity;

import java.util.List;

public class InfoAdapter extends RecyclerView.Adapter<InfoAdapter.Myholder> {
    List<ContactDetails> dataModelArrayList;
    private  onItemEditListener itemEditListener;
    public InfoAdapter(HomeActivity homeActivity, List<ContactDetails> dataModelArrayList, onItemEditListener itemEditListener) {
        this.dataModelArrayList = dataModelArrayList;
        this.itemEditListener =  itemEditListener;
        Log.d("readData","size AD "+dataModelArrayList.size());

    }

    class Myholder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_name,tv_mob;
        LinearLayout lnr_main_layout;
        public Myholder(View itemView) {
            super(itemView);

            tv_name =  itemView.findViewById(R.id.tv_name);
            tv_mob =  itemView.findViewById(R.id.tv_mob);
            lnr_main_layout = itemView.findViewById(R.id.lnr_main_layout);

            lnr_main_layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.lnr_main_layout:
                    int pos = getLayoutPosition();
                    itemEditListener.loadEditText(dataModelArrayList.get(pos));
                    break;
            }
        }
    }


    @Override
    public Myholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row,null);
        return new Myholder(view);

    }

    @Override
    public void onBindViewHolder(Myholder holder, int position) {
        ContactDetails dataModel=dataModelArrayList.get(position);
        holder.tv_name.setText(dataModel.getName());
        holder.tv_mob.setText(dataModel.getContact());

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    public void updateList(List<ContactDetails> updateList){
        dataModelArrayList.clear();
        dataModelArrayList = updateList;
        notifyDataSetChanged();
        Log.d("readData","size ADP "+dataModelArrayList.size());

    }
    public void updateData(ContactDetails details){
        dataModelArrayList.add(0,details);
        notifyDataSetChanged();
        Log.d("readData","size ADF"+dataModelArrayList.size());

    }

    public interface onItemEditListener{
        void loadEditText(ContactDetails details);
    }
}
