package com.example.doodletask.model;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {

    public static final String EMAIL_ID = "email_id";
    public static String KEY_SIGNED_IN = "is_signin";


    private static SharedPreference mManageSharedPreferences = null;


    private SharedPreference() {

    }

    public static synchronized SharedPreference newInstance(){
        if(mManageSharedPreferences == null){
            mManageSharedPreferences = new SharedPreference();
        }

        return mManageSharedPreferences;
    }

    /**
     * Method to save the boolean value
     * @param context
     * @param value
     * @param key
     */
    public void saveBoolean(Context context, boolean value , String key) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(key, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2
        editor.putBoolean(key, value); //3
        editor.commit(); //4
    }


    /**
     * Method to get the string value.
     * @param context
     * @return
     */
    public boolean getBoolean(Context context, String key) {
        SharedPreferences settings;
        settings = context.getSharedPreferences(key, Context.MODE_PRIVATE); //1
        return settings.getBoolean(key, false); //2;
    }
    /**
     * Method to save the string value.
     * @param context
     * @param text
     */
    public void saveString(Context context, String text , String key) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(key, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2
        editor.putString(key, text); //3
        editor.commit(); //4
    }


    /**
     * Method to get the string value.
     * @param context
     * @return
     */
    public String getString(Context context, String key) {
        SharedPreferences settings;
        String text;
        settings = context.getSharedPreferences(key, Context.MODE_PRIVATE); //1
        text = settings.getString(key, null); //2
        return text;
    }

}


