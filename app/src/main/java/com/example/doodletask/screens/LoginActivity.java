package com.example.doodletask.screens;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doodletask.R;
import com.example.doodletask.database.MyDataBase;
import com.example.doodletask.model.ContactDetails;
import com.example.doodletask.model.SharedPreference;
import com.example.doodletask.model.SignupDetails;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText et_email, et_pass;
    TextView tv_login, tv_signup;
    MyDataBase dataBase;
    SignupDetails signupDetails;
    public static final String PREFS_NAME = "MyPrefsFile";
    SharedPreference preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        et_email = findViewById(R.id.email);
        et_pass = findViewById(R.id.pass);
        tv_login = findViewById(R.id.login);

        tv_signup = findViewById(R.id.signup);
        tv_login.setOnClickListener(this);
        tv_signup.setOnClickListener(this);

        preferences = SharedPreference.newInstance();
        dataBase= new MyDataBase(this);
        signupDetails = new SignupDetails();


        if(preferences.getBoolean(LoginActivity.this,SharedPreference.KEY_SIGNED_IN))
        {
            startActivity(new Intent(LoginActivity.this,HomeActivity.class));
            finish();
        }

    }

    private boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    private void dialogSignUp() {
        LayoutInflater inflater = LayoutInflater.from(LoginActivity.this);
        View dialogView = inflater.inflate(R.layout.signup, new LinearLayout(LoginActivity.this), true);
        final EditText user = dialogView.findViewById(R.id.username);
        final EditText email = dialogView.findViewById(R.id.et_email);
        final EditText pass = dialogView.findViewById(R.id.pass);
        final EditText con_pass = dialogView.findViewById(R.id.con_pass);
        TextView tv_signup = dialogView.findViewById(R.id.tv_signup);

        final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this)
                .setView(dialogView)
                .create();

        DisplayMetrics metrics = new DisplayMetrics();
        int nWidth = metrics.widthPixels;
        int nHeight = metrics.heightPixels;
        dialog.getWindow().setLayout(nWidth, nHeight);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (user.getText().toString().equals("")) {
                    user.setError("User name is mandatory");

                } else if (!isValidEmailId(email.getText().toString().trim())) {
                    email.setError("InValid Email Address");
                } else if (pass.getText().toString().length() < 6 || !isValidPassword(pass.getText().toString())) {
                    pass.setError("Please enter valid password...with minimum 6 character atlest 1 uppercase, 1 numeric, lowercase, and 1 special character");
                } else if (!(con_pass.getText().toString()).equals(pass.getText().toString())) {
                    con_pass.setError("password and confirm password are not matching");
                } else {

                    SignupDetails signupDetails = new SignupDetails();
                    signupDetails.setUserName(user.getText().toString().trim());
                    signupDetails.setUserEmail(email.getText().toString().trim());
                    signupDetails.setUserPassword(pass.getText().toString().trim());
                    dataBase.insertUserSignupData(signupDetails);

                    preferences.saveBoolean(LoginActivity.this,true,SharedPreference.KEY_SIGNED_IN);
                    preferences.saveString(LoginActivity.this,signupDetails.getUserEmail(),SharedPreference.EMAIL_ID);
                    dialog.dismiss();
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                }
            }
        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.signup:
                dialogSignUp();
                break;

            case R.id.login:

                if (!isValidEmailId(et_email.getText().toString().trim())) {
                    et_email.setError("InValid Email Address");
                } else if (et_pass.getText().toString().length() < 6 || !isValidPassword(et_pass.getText().toString())) {
                    et_pass.setError("Please enter valid password...with minimum 6 character atlest 1 uppercase, 1 numeric, lowercase, and 1 special character");
                } else {
                    SignupDetails signupDetails = dataBase.readSignUpDetails(et_email.getText().toString().trim());
                   // dataBase.readSignUpDetails(et_email.getText().toString().trim());
                    if (signupDetails!= null && signupDetails.getUserEmail().equalsIgnoreCase(et_email.getText().toString().trim()) &&
                    signupDetails.getUserPassword().equalsIgnoreCase(et_pass.getText().toString().trim())){
                        preferences.saveBoolean(LoginActivity.this,true,SharedPreference.KEY_SIGNED_IN);
                        preferences.saveString(LoginActivity.this,signupDetails.getUserEmail(),SharedPreference.EMAIL_ID);

                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                        finish();
                    }
                    else{
                        Toast.makeText(this, "Invalide email or password", Toast.LENGTH_SHORT).show();
                    }
                }

        }
    }
}


