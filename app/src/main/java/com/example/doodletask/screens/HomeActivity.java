package com.example.doodletask.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.doodletask.adapter.InfoAdapter;
import com.example.doodletask.database.MyDataBase;
import com.example.doodletask.model.ContactDetails;
import com.example.doodletask.R;
import com.example.doodletask.model.SharedPreference;
import com.example.doodletask.model.SignupDetails;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements InfoAdapter.onItemEditListener {
    BottomSheetDialog bottomSheetDialog;
    FloatingActionButton floatingActionButton;
    String name,mobile;
    private List<ContactDetails> infoList = new ArrayList<>();
    private InfoAdapter mAdapter;
    RecyclerView recyclerView;
    MyDataBase dataBase;
    String userEmail;
    TextView tv_logout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        floatingActionButton = findViewById(R.id.fab);
        recyclerView = findViewById(R.id.rv_info);
        tv_logout = findViewById(R.id.tv_logout);

        dataBase = new MyDataBase(this);

       // infoList.addAll(dataBase.getAllInfo());

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowBottomSheet(false,null,-1);

            }
        });
         userEmail = SharedPreference.newInstance().getString(HomeActivity.this,SharedPreference.EMAIL_ID);
//        SignupDetails signupDetails = dataBase.readSignUpDetails(userEmail);
       if (dataBase.readContactByNumber(userEmail) != null){
           infoList = dataBase.readContacts(userEmail);

       }

        mAdapter = new InfoAdapter(this,infoList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreference.newInstance().saveBoolean(HomeActivity.this,false,SharedPreference.KEY_SIGNED_IN);
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

            }
        });




    }
    private void ShowBottomSheet(final boolean shouldUpdate, final ContactDetails dataModel, final int position) {


        View view = getLayoutInflater().inflate(R.layout.bottomsheet_layout, null);
        final EditText et_name = view.findViewById(R.id.et_name);
        final EditText et_mobile = view.findViewById(R.id.et_mobile);
        Button btn_save = view.findViewById(R.id.btn_save);
        Button btn_delete = view.findViewById(R.id.btn_delete);
        Button btn_edit = view.findViewById(R.id.btn_edit);

        if (shouldUpdate){
            et_name.setText(dataModel.getName());
            et_mobile.setText(dataModel.getContact());
            btn_delete.setVisibility(View.VISIBLE);
            btn_edit.setVisibility(View.VISIBLE);
            btn_save.setVisibility(View.GONE);
         //   btn_save.setVisibility(View.VISIBLE);

            btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dataBase.deleteByNumber(dataModel.getContact());
                    mAdapter.updateList(dataBase.readContacts(userEmail));
                    bottomSheetDialog.dismiss();
                }
            });

           btn_edit.setOnClickListener(new View.OnClickListener() {
        //    btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ContactDetails contactDetails = new ContactDetails();
                    contactDetails.setName(et_name.getText().toString().trim());
                    contactDetails.setContact(et_mobile.getText().toString().trim());

                    dataBase.updateDatabase(contactDetails,dataModel.getContact());

                    mAdapter.updateList(dataBase.readContacts(userEmail));
                    bottomSheetDialog.dismiss();

                }
            });

        }else {
            btn_delete.setVisibility(View.GONE);
            btn_edit.setVisibility(View.GONE);
            btn_save.setVisibility(View.VISIBLE);
            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    ContactDetails contactDetails = new ContactDetails();
                    contactDetails.setName(et_name.getText().toString().trim());
                    contactDetails.setContact(et_mobile.getText().toString().trim());
                    dataBase.insertData(contactDetails,userEmail);

                    infoList = dataBase.readContacts(userEmail);
                    mAdapter.updateData(contactDetails);
                    Log.d("readData","size "+infoList.size());
                    et_mobile.setText("");
                    et_name.setText("");

                }
            });
        }

        bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();
    }






    /**
     * Updating note in db and updating
     * item in the list by its position
     */
    private void updateNote(String note, int position) {
        ContactDetails n = infoList.get(position);
        // updating note text
        n.setContact(note);

        // updating note in db
     //   dataBase.updateNote(n);

        // refreshing the list
        infoList.set(position, n);
        mAdapter.notifyItemChanged(position);

    }




    private void showActionsDialog(final int position) {
        CharSequence colors[] = new CharSequence[]{"Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose option");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    ShowBottomSheet(true, infoList.get(position), position);
                } else {
                    //deleteInfo(position);
                }
            }
        });
        builder.show();
    }


    @Override
    public void loadEditText(ContactDetails details) {
        ShowBottomSheet(true,details,0);
    }
}
